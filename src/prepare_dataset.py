'''
module: main
package: experiments
author: machadowisck
date: 2024

Prepare and split datasets(s)
'''


from pathlib import Path
import pandas as pd
from tqdm import tqdm
from experiments.utils import (clear_missing_files,
                               load_valid_audio_file,
                               extract_validate_split
                               )
# TODO : logging

def prepare_train_dataset_propor(base_path="/content/data",
                                 extension=".wav"):
    """
    prepares SER@PROPOR2022 train dataset
    """
    data = []
    input_files_extension = extension
    train_path = base_path+"/train"

    # train SET  # SER@PROPOR2022 challenge Shared Task
    for path in tqdm(Path(train_path).glob("**/*"+input_files_extension)):
        name = str(path).split('/')[-1].split('.')[0]
        bid, segment, label = str(name).split('_')
        # gender = subject[0]
        # text_read = str(name).split('_')[1]
        # audio_category = text_read[0]
        # label = str(name).split('-')[0]

        try:
            # There are some broken files
            load_valid_audio_file(path)
            data.append({
                "name": name,
                "path": path,
                "label": label,
                "bid": bid,
                "segment": segment,
                "wav_file": '',
                "emotion": label
            })
        except Exception as e:
            print(str(path), e) # log this
            continue

    train_df = pd.DataFrame(data)
    train_df = clear_missing_files(train_df)
    train_df.to_csv(f"{base_path}/train.csv",
                    sep="\t",
                    encoding="utf-8",
                    index=False)

    return train_df


def prepare_test_dataset_propor(base_path="/content/data",
                                extension=".wav"):
    """
    prepares SER@PROPOR2022 train dataset
    """
    test_data = []
    input_files_extension = extension
    test_path = base_path+"/test"

    # TEST SET
    # load test_metadata
    try:
        test_metadata = pd.read_csv(base_path+"/test_ser_metadata.csv")
    except:
        # log error
        return pd.DataFrame()

    # process wav files
    test_path += "/test_ser"
    for path in tqdm(Path(test_path).glob("**/*"+input_files_extension)):
        wav_file_name = str(path).split('/')[-1]

        row = test_metadata.loc[
            test_metadata["wav_file"] == wav_file_name
        ].to_dict(orient="records")[0]

        name = row['file'].split('.')[0]
        bid, segment, label = str(name).split('_')

        try:
            # There are some broken files
            load_valid_audio_file(path)
            test_data.append({
                "name": name,
                "path": path,
                "label": label,
                "bid": bid,
                "segment": segment,
                "wav_file": wav_file_name,
                "emotion": label
            })
        except Exception as e:
            print(str(path), e) # TODO log this
            continue

    test_df = pd.DataFrame(test_data)
    test_df = clear_missing_files(test_df)
    test_df.to_csv(f"{base_path}/test.csv",
                   sep="\t",
                   encoding="utf-8",
                   index=False,
                   quotechar="'")
    return test_df

def prepare_dataset_propor():
    """
    module function to be called if no function is specified
    """
    train_df = prepare_train_dataset_propor()
    prepare_test_dataset_propor()

    train_df, validate_df = extract_validate_split(train_df,
                                                   test_size=0.2666)



if __name__ == '__main__':
    prepare_dataset_propor()
