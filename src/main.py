'''
module: main
package: experiments
author: machadowisck
date: 2024

main classes for Speech Emotion Recognition (SER)  in portuguese PT-BR
'''

import torchaudio
import librosa
import datasets
from datasets import load_dataset, load_metric
import os
import numpy as np

import torch
import sys
from IPython import display as disp


from denoiser import pretrained
from denoiser.dsp import convert_audio

from transformers import AutoConfig, Wav2Vec2Processor

class AudioExperiment:
    '''
    Class to hold general info about the experiments pipeline
    '''
    def __init__(self,
                 input_column: str = 'path',
                 output_column: str = 'emotion',
                 model_name_or_path: str = ''):

        self.input_column = input_column
        self.output_column = output_column


class AudioDataset(datasets.Dataset):
    def __init__(self, *args, **kwargs):
        self.datasets_dict = {}
        super().__init__(args, kwargs)
