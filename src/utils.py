'''
module: utils
package: experiments
author: machadowisck
date: 2024

Prepare and split datasets(s)
'''

import os
import torchaudio
from sklearn.model_selection import train_test_split
import librosa
import numpy as np
import pandas as pd
from denoiser import pretrained
from denoiser.dsp import convert_audio
import torch


def clear_missing_files(full_df):
    """removes missing path lines from dataset"""
    full_df["status"] = full_df["path"].apply(
        lambda path: True if os.path.exists(path) else None)
    full_df = full_df.dropna(subset=["path"])
    full_df = full_df.drop("status", 1)

    full_df = full_df.sample(frac=1)
    full_df = full_df.reset_index(drop=True)
    return full_df


def speech_file_to_array_fn(path: str, target_sr=16_000):
    """converts audio file to numpy array in specified sample_rate"""
    speech_array, sampling_rate = torchaudio.load(path)
    speech = speech_array[0].numpy().squeeze()

    '''
    resampler = torchaudio.transforms.Resample(sampling_rate,
                                               target_sr)
    speech = resampler(speech_array).squeeze().numpy()
    '''
    speech = librosa.resample(np.asarray(speech),
                              orig_sr=sampling_rate,
                              target_sr=target_sr)
    return speech


def load_valid_audio_file(path: str):
    """returns a tuple (speech_array, sampling_rate) or Exception"""
    return torchaudio.load(path)


def speech_file_to_array_denoise_fn(path):
    '''
    wav, sr = torchaudio.load('alex_noisy.mp3')
    wav = convert_audio(wav.cuda(), sr, model.sample_rate, model.chin)
    with torch.no_grad():
        denoised = model(wav[None])[0]
    disp.display(disp.Audio(wav.data.cpu().numpy(),
                 rate=model.sample_rate))
    disp.display(disp.Audio(denoised.data.cpu().numpy(),
                 rate=model.sample_rate))
    '''
    model = pretrained.dns64().cuda()
    speech_array, sampling_rate = torchaudio.load(path)

    # speech = speech_array[0].numpy().squeeze()
    speech = speech_array

    '''speech = librosa.resample(np.asarray(speech),
                              orig_sr=sampling_rate,
                              target_sr=16_000)

    wav = convert_audio(wav.cuda(),
                        sr,
                        model.sample_rate,
                        model.chin)
    '''
    speech = convert_audio(speech.cuda(),
                           sampling_rate,
                           model.sample_rate,
                           model.chin)
    with torch.no_grad():
        speech = model(speech[None])[0]

    '''
    disp.display(disp.Audio(speech_array.data.cpu().numpy(),
                            rate=model.sample_rate))
    disp.display(disp.Audio(speech.data.cpu().numpy(),
                            rate=model.sample_rate))
    '''
    speech = speech.data.cpu().numpy().squeeze()

    speech = librosa.resample(np.asarray(speech),
                              orig_sr=sampling_rate,
                              target_sr=16_000)
    # disp.display(disp.Audio(speech, rate=model.sample_rate))
    return speech


def extract_validate_split(df,
                           test_size=0.25,
                           random_state=101,
                           stratify=["emotion"],
                           shuffle=True):
    return train_test_split(df,
                            test_size=test_size,
                            random_state=random_state,
                            stratify=pd.DataFrame(df,
                                                  columns=stratify),
                            shufle=shuffle,
                            )
